package ru.book;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.book.models.*;
import ru.book.repository.HistoryRepo;
import ru.book.repository.PublisherRepo;
import ru.book.repository.UserRepo;
import ru.book.service.ActionService;
import ru.book.service.GenresService;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class test1 {
    @Autowired
    private HistoryRepo historyRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    ActionService actionService;
    @Autowired
    GenresService genresService;
    @Autowired
    PublisherRepo publisherRepo;

    @Test
    public void findUser(){
        Users user = userRepo.findByEmail("ssss@mail.ru");
        System.out.println("usr: " + user.getLastName());
    }

    @Test
    public void findSharedBooksByUser(){
        Users user = userRepo.findByEmail("ssss@mail.ru");
        Actions action1 = actionService.findById((long) 1);
        Actions action2 = actionService.findById((long) 3);
        List<Histories> histories =  historyRepo.findSharedBooksByUser(action1, action2, user);
        for (Integer i = 0; i<histories.size(); i++)
            System.out.println(i.toString() + ") " + histories.get(i).getBook().getTitle()
                    + ". " + histories.get(i).getAction().getAction()
                    + " " + histories.get(i).getOrderDate());
    }

    @Test
    public void findSharedBooksByGenre(){
        Actions action1 = actionService.findById((long) 1);
        Actions action2 = actionService.findById((long) 3);
        Genres genre  = genresService.findById((long) 2);
        List<Histories> histories =  historyRepo.findSharedBooksByGenre(action1, action2, genre);
        for (Integer i = 0; i<histories.size(); i++)
            System.out.println(i.toString() + ") " + histories.get(i).getBook().getTitle()
                    + ". Жанр: " + histories.get(i).getBook().getGenre().getGenre()
                    + ". " + histories.get(i).getAction().getAction()
                    + " " + histories.get(i).getOrderDate());
    }

    @Test
    public void findSharedBooksByGenreAndPublishers(){
        Actions action1 = actionService.findById((long) 1);
        Actions action2 = actionService.findById((long) 3);
        Genres genre  = genresService.findById((long) 1);
        Publishers publisher = publisherRepo.getOne((long)2);
        List<Histories> histories =  historyRepo.findSharedBooksByGenreAndPublishers(action1, action2, genre, publisher);
        for (Integer i = 0; i<histories.size(); i++)
            System.out.println(i.toString() + ") " + histories.get(i).getBook().getTitle()
                    + ". Жанр: " + histories.get(i).getBook().getGenre().getGenre()
                    + ". " + histories.get(i).getAction().getAction()
                    + " " + histories.get(i).getOrderDate());
    }
}
