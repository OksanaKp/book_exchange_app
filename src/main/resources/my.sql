-- выбор раздаваемых в данное время книг (отаднных новых и возвращенных им)
SELECT * FROM  (
                   select h1.id, book_id, action_id, orderdate, user_id
                   from history h1
                   where orderdate = (
                       select max(h2.orderdate)
                       from history h2
                       where h1.book_id = h2.book_id
                   )
                   order by book_id
               ) as h
where (action_id = 1 or action_id = 3) and user_id = 2;

-- запрос для каталога (выбор всех добавленный и находящихся на раздаче
-- книг (кроме читаемых в настоящее время))
SELECT * FROM  (
                   select h1.id, book_id, action_id, orderdate, user_id
                   from history h1
                   where orderdate = (
                       select max(h2.orderdate)
                       from history h2
                       where h1.book_id = h2.book_id
                   )
                   order by book_id
               ) as h
where action_id = 1 or action_id = 3;

-- выбор книг, находящихся на чтении у определенного пользователя
SELECT * FROM  (
                   select h1.id, book_id, action_id, orderdate, user_id
                   from history h1
                   where orderdate = (
                       select max(h2.orderdate)
                       from history h2
                       where h1.book_id = h2.book_id
                   )
                   order by book_id
               ) as h
where action_id = 2 and user_id = 2;
