create database newbooks;

insert into users (id, mail, firstname, lastname, passwords, roles)
values (1,'admin@mail.ru', 'admin', 'admin', '111', 'admin');
insert into users (id, mail, firstname, lastname, passwords, roles)
values (2,'ssss@mail.ru', 'Федор', 'Фин', '111', 'user');
insert into users (id, mail, firstname, lastname, passwords, roles)
values (3,'ssss2@mail.ru', 'Ольга', 'Фин', '111', 'user');


insert into genres (id, genre) values (1,'Художественная литература');
insert into genres (id, genre) values (2,'Наука и техника');


insert into publishers (id, publisher, town) values (1,'АСТ', 'Москва');
insert into publishers (id, publisher, town) values (2,'Вече', 'Москва');
insert into publishers (id, publisher, town) values (3,'Физматкнига', 'Москва');

insert into books (id, author, page_count, title, genre_id, publisher_id)
values (1,'Дюма А.', 672, 'Три мушкетера', 1, 1);
insert into books (id, author, page_count, title, genre_id, publisher_id)
values (2,'Верн Ж.', 480, 'Кловис Дардантор. Зеленый луч', 1, 2);
insert into books (id, author, page_count, title, genre_id, publisher_id)
values (3,'Стивенсон Р.', 448, 'Черная стрела. Остров сокровищ', 1, 2);
insert into books (id, author, page_count, title, genre_id, publisher_id)
values (4,'Курбатов Л.', 300, 'Оптоэлектроника видимого и инфракрасного диапазонов спектра', 1, 3);
insert into books (id, author, page_count, title, genre_id, publisher_id)
values (5,'Косарев В.', 240, '12 лекций по вычислительной математике. Вводный курс', 1, 3);


insert  into actions (id, action) values (1,'Внесена в базу');
insert  into actions (id, action) values (2,'Читается');
insert  into actions (id, action) values (3,'Возвращена на раздачу');


insert  into history (id, orderdate, action_id, book_id, user_id)
values (1,'2020-01-20 03:42:59.000000', 1, 1, 1);
insert  into history (id, orderdate, action_id, book_id, user_id)
values (2,'2020-01-21 03:42:59.000000', 1, 2, 2);
insert  into history (id, orderdate, action_id, book_id, user_id)
values (3,'2020-01-22 03:42:59.000000', 1, 3, 2);
insert  into history (id, orderdate, action_id, book_id, user_id)
values (4,'2020-01-23 03:42:59.000000', 2, 1, 2);
insert  into history (id, orderdate, action_id, book_id, user_id)
values (5,'2020-01-24 03:42:59.000000', 1, 4, 3);
insert  into history (id, orderdate, action_id, book_id, user_id)
values (6,'2020-01-25 03:42:59.000000', 1, 5, 3);
insert  into history (id, orderdate, action_id, book_id, user_id)
values (7,'2020-01-26 03:42:59.000000', 3, 1, 2);
insert  into history (id, orderdate, action_id, book_id, user_id)
values (8,'2020-01-27 03:42:59.000000', 2, 5, 2);
insert  into history (id, orderdate, action_id, book_id, user_id)
values (9,'2020-01-28 03:42:59.000000', 2, 1, 2);

