package ru.book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.book.models.Genres;
import ru.book.repository.GenreRepo;

import java.util.List;

@Service
public class GenresService {

    @Autowired
    GenreRepo genreRepo;

    public List<Genres> getAll() {
        return genreRepo.findAll();
    }

    public void save(Genres genre) {
        genreRepo.save(genre);
    }
    public Genres getById(Long id) {
        return genreRepo.getOne(id);
    }

    public Genres findById(Long id) {
        Genres genres = new Genres(genreRepo.getOne(id));
        return genres;
    }
}
