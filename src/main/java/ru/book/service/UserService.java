package ru.book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.book.models.Users;
import ru.book.repository.UserRepo;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepo userRepo;

    public List<Users> getAll() {
        List<Users> users = userRepo.findAll();
        return users;
    }

    public Users findByEmail(String mailLoggedUser) {
        Users user = userRepo.findByEmail(mailLoggedUser);
        return user;
    }
    public void save(Users user) {
        userRepo.save(user);
    }

    public Users findById(Long id) {
        Users user = new Users(userRepo.getOne(id));
        return user;
    }

    public List<Users> findByLastName(String lastName) {
        return userRepo.findByLastName(lastName);
    }

    public List<Users> findAll() {
        return userRepo.findAll();
    }

}
