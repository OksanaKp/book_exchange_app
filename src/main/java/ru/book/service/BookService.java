package ru.book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.book.models.Books;
import ru.book.models.Genres;
import ru.book.models.Publishers;
import ru.book.repository.BookRepo;

import java.util.List;

@Service
public class BookService {
    @Autowired
    private BookRepo bookRepo;

    public List<Books> getAll() {
        List<Books> books = bookRepo.findAll();
        return books;
    }

    public Books findById(Long id) {
        Books book = new Books(bookRepo.getOne(id));
        return book;
    }

    public List<Books>  findByGenre(Genres genre) {
        List<Books>  books= bookRepo.findByGenre(genre);
        return books;
    }

    public List<Books>  findByPublisher(Publishers publisher) {
        List<Books>  books= bookRepo.findByPublisher(publisher);
        return books;
    }

    public void save(Books book) {
        bookRepo.save(book);
    }

    public Iterable<Books> findByGenreAndPublisher(Genres genre, Publishers publisher) {
        List<Books>  books= bookRepo.findByGenreAndPublisher(genre, publisher);
        return books;
    }

    public void deleteById(Long id) {
        bookRepo.deleteById(id);
    }
}
