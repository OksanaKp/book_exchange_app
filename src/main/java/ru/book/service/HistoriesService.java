package ru.book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.book.models.*;
import ru.book.repository.HistoryRepo;

import java.util.List;

@Service
public class HistoriesService {
    @Autowired
    private HistoryRepo historyRepo;
    @Autowired
    ActionService actionService;

    public List<Histories> getAll() {
        List<Histories> histories = historyRepo.findAll();
        return histories;
    }

    public List<Histories> findByUser(Users user) {
        List<Histories> histories =  historyRepo.findByUser(user);
        return histories;
    }

    public List<Histories> findByUserAndAction(Users user, Actions action) {
        List<Histories> histories =  historyRepo.findByUserAndAction(user, action);
        return histories;
    }

    public void save(Histories history) {
        historyRepo.save(history);
    }

    public List<Histories> findSharedBooksByUser(Users continueUser) {
        Actions action1 = actionService.findById((long) 1);
        Actions action2 = actionService.findById((long) 3);
        return historyRepo.findSharedBooksByUser(action1, action2, continueUser);
    }

    public List<Histories> findByUserAndLastAction(Users continueUser, Actions action) {
        return historyRepo.findByUserAndLastAction(action, continueUser);
    }


    public List<Histories> findSharedBooks(Actions action1, Actions action2) {
        return historyRepo.findSharedBooks(action1, action2);
    }

    public List<Histories> findSharedBooksByGenre(Actions action1, Actions action2, Genres genre) {
        return historyRepo.findSharedBooksByGenre(action1, action2, genre);
    }

    public List<Histories> findSharedBooksByPublishers(Actions action1, Actions action2, Publishers publisher) {
        return historyRepo.findSharedBooksByPublishers(action1, action2, publisher);
    }

    public List<Histories> findSharedBooksByGenreAndPublishers(Actions action1, Actions action2, Genres genref, Publishers publisher) {
        return historyRepo.findSharedBooksByGenreAndPublishers(action1, action2, genref, publisher);
    }

}
