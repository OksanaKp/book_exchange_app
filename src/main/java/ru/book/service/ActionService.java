package ru.book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.book.models.Actions;
import ru.book.repository.ActionRepo;

import java.util.List;

@Service
public class ActionService {
    @Autowired
    private ActionRepo actionRepo;

    public List<Actions> getAll() {
        List<Actions> actions = actionRepo.findAll();
        return actions;
    }

    public Actions findById(Long id) {
        Actions actions = new Actions(actionRepo.getOne(id));
        return actions;
    }
    public void save(Actions action) {
        actionRepo.save(action);
    }
}
