package ru.book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.book.models.Publishers;
import ru.book.repository.PublisherRepo;

import java.util.List;

@Service
public class PublisherService {

    @Autowired
    PublisherRepo publisherRepo;

    public List<Publishers> getAll() {
        return publisherRepo.findAll();
    }

    public void save(Publishers publisher) {
        publisherRepo.save(publisher);
    }
    public Publishers getById(Long id) {
        return publisherRepo.getOne(id);
    }
}
