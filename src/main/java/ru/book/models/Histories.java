package ru.book.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "history")
public class Histories {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "orderdate")
    private Date orderDate;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Users user;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id")
    private Books book;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "action_id")
    private Actions action;

    public Histories() {
    }

    public Histories(Date orderDate, Users user, Books book, Actions action) {
        this.orderDate = orderDate;
        this.user = user;
        this.book = book;
        this.action = action;
    }

    public Histories(Histories histories) {
        this.id = histories.getId();
        this.orderDate = histories.getOrderDate();
        this.user = histories.getUser();
        this.book = histories.getBook();
        this.action = histories.getAction();
    }

    public Histories(Users user, Books book, Actions action, Date date) {
        this.orderDate = date;
        this.user = user;
        this.book = book;
        this.action = action;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Books getBook() {
        return book;
    }

    public void setBook(Books book) {
        this.book = book;
    }

    public Actions getAction() {
        return action;
    }

    public void setAction(Actions action) {
        this.action = action;
    }
}
