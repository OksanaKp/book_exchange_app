package ru.book.models;

import javax.persistence.*;

@Entity
@Table(name = "actions")
public class Actions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String action;

    public Actions() {
    }

    public Actions(Actions action) {
        this.id = action.getId() ;
        this.action = action.getAction();
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }

}
