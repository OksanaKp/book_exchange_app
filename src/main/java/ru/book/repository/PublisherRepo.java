package ru.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.book.models.Publishers;

public interface PublisherRepo extends JpaRepository<Publishers, Long> {

}
