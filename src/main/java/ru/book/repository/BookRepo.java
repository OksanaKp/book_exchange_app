package ru.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.book.models.*;

import java.util.List;

public interface BookRepo extends JpaRepository<Books, Long> {
    List<Books>  findByGenre(Genres genre);
    List<Books>  findByPublisher(Publishers publisher);
    List<Books> findByGenreAndPublisher(Genres genre, Publishers publisher);
}
