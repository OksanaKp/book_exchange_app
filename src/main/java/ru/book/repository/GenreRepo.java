package ru.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.book.models.Genres;

public interface GenreRepo extends JpaRepository<Genres, Long> {
}
