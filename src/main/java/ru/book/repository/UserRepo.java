package ru.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.book.models.Users;

import java.util.List;

public interface UserRepo extends JpaRepository<Users, Long> {
    Users findByEmail(String mail);

    List<Users> findByLastName(String lastName);

}
