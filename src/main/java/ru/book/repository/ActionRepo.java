package ru.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.book.models.Actions;

public interface ActionRepo extends JpaRepository<Actions, Long> {

}
