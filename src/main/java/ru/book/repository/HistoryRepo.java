package ru.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.book.models.*;

import java.util.List;

public interface HistoryRepo extends JpaRepository<Histories, Long> {
    List<Histories> findByUser(Users user);
    List<Histories> findByUserAndAction(Users user, Actions action);


    //только для теста
    @Query("select h1.id, h1.book, h1.action, h1.orderDate, h1.user " +
            "from Histories h1 " +
            "where h1.orderDate = ( " +
            "    select max(h2.orderDate) " +
            "    from Histories h2\n" +
            "    where h1.book = h2.book\n" +
            ") and (h1.action = ?1)\n" +
            "order by h1.book")
    List<Histories> findSharedBooks1(@Param ("act") Actions act);

    //используется для вывода списка книг в разделе "раздаваемые мной сейчас"
    //сюда входят книги только что добавленные или возвращенные на раздачу в настоящее время
    @Query("select h1 " +
            "from Histories h1 " +
            "where h1.orderDate = ( " +
            "    select max(h2.orderDate) " +
            "    from Histories h2\n" +
            "    where h1.book = h2.book\n" +
            ") and (h1.action = ?1 or h1.action = ?2) and h1.user = ?3\n" +
            "order by h1.book")
    List<Histories> findSharedBooksByUser(@Param ("act") Actions act, @Param ("act2") Actions act2,
                                          @Param ("usr") Users usr);

    //используется для вывода списка книг в разделе "Сейчас у меня"
    //список книг, взятых на чтение из общей раздачи, никем не занятых в настоящее время
    @Query("select h1 " +
            "from Histories h1 " +
            "where h1.orderDate = ( " +
            "    select max(h2.orderDate) " +
            "    from Histories h2\n" +
            "    where h1.book = h2.book\n" +
            ") and (h1.action = ?1) and h1.user = ?2\n" +
            "order by h1.book")
    List<Histories> findByUserAndLastAction( @Param ("act") Actions act, Users continueUser);

///////////////////////////////////////////////////////////////////////////////////////////
//используется для вывода списка книг в разделе "раздаваемые мной сейчас"
//сюда входят книги только что добавленные или возвращенные на раздачу в настоящее время
    @Query("select h1 " +
            "from Histories h1 " +
            "where h1.orderDate = ( " +
            "    select max(h2.orderDate) " +
            "    from Histories h2\n" +
            "    where h1.book = h2.book\n" +
            ") and (h1.action = ?1 or h1.action = ?2)\n" +
            "order by h1.book")
    List<Histories> findSharedBooks(@Param ("act") Actions act, @Param ("act2") Actions act2);

    @Query("select h1 " +
            "from Histories h1 " +
            "where h1.orderDate = ( " +
            "    select max(h2.orderDate) " +
            "    from Histories h2, Books b2\n" +
            "    where h1.book = h2.book\n" +
            "and (h1.book = b2) and b2.genre = ?3" +

            ") and (h1.action = ?1 or h1.action = ?2) " +
            "" +
            "order by h1.book")
    List<Histories> findSharedBooksByGenre(@Param ("act") Actions act, @Param ("act2") Actions act2,
                                           @Param ("genre") Genres genre);


    @Query("select h1 " +
            "from Histories h1 " +
            "where h1.orderDate = ( " +
            "    select max(h2.orderDate) " +
            "    from Histories h2, Books b2\n" +
            "    where h1.book = h2.book\n" +
            "and (h1.book = b2) and b2.publisher = ?3" +

            ") and (h1.action = ?1 or h1.action = ?2) " +
            "" +
            "order by h1.book")
    List<Histories> findSharedBooksByPublishers(@Param ("act") Actions act, @Param ("act2") Actions act2,
                                           @Param ("genre") Publishers publisher);


    @Query("select h1 " +
            "from Histories h1 " +
            "where h1.orderDate = ( " +
            "    select max(h2.orderDate) " +
            "    from Histories h2, Books b2\n" +
            "    where h1.book = h2.book\n" +
            "and (h1.book = b2) and b2.genre = ?3 and b2.publisher = ?4" +

            ") and (h1.action = ?1 or h1.action = ?2) " +
            "" +
            "order by h1.book")
    List<Histories> findSharedBooksByGenreAndPublishers(@Param ("act") Actions act, @Param ("act2") Actions act2,
                                                @Param ("genre") Genres genre,
                                                @Param ("genre") Publishers publisher);

}

