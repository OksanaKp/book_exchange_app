package ru.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.book.models.Actions;
import ru.book.models.Histories;
import ru.book.models.Users;
import ru.book.service.ActionService;
import ru.book.service.BookService;
import ru.book.service.HistoriesService;
import ru.book.service.UserService;

import java.util.List;

@Controller
public class BooksHistoryController {

    @Autowired
    UserService userService;

    @Autowired
    HistoriesService historiesService;

    @Autowired
    BookService bookService;

    @Autowired
    ActionService actionService;

    private Users getCurrentLoggedUser() {
        Authentication authenticationUser = SecurityContextHolder.getContext().getAuthentication();
        String mailLoggedUser = authenticationUser.getName();
        Users continueUser = userService.findByEmail(mailLoggedUser);
        return continueUser;
    }

    @RequestMapping(value = { "/user/distributed" }, method = RequestMethod.GET)
    public String userDistributed(Model model) {
        Users continueUser = getCurrentLoggedUser();
        Actions action = actionService.findById((long) 3);
        List<Histories> histories =  historiesService.findSharedBooksByUser(continueUser);
        model.addAttribute("userHistories", histories);
        Attributes.setAttributes(continueUser, model);
        return "user/distributedbooks";
    }

    @RequestMapping(value = { "/user/gotten" }, method = RequestMethod.GET)
    public String userGotten(Model model) {
        Users continueUser = getCurrentLoggedUser();
        Actions action2 = actionService.findById((long) 2);
        List<Histories> histories =  historiesService.findByUserAndLastAction(continueUser, action2);
        model.addAttribute("userHistories", histories);
        Attributes.setAttributes(continueUser, model);
        return "user/gottenbooks";
    }

 //---------------------------------------------------------------------------------------------
    @RequestMapping(value = { "/user/added" }, method = RequestMethod.GET)
    public String userAdded(Model model) {
        Users continueUser = getCurrentLoggedUser();
        Actions action = actionService.findById((long) 1);
        List<Histories> histories =  historiesService.findByUserAndAction(continueUser, action);
        model.addAttribute("userHistories", histories);
        Attributes.setAttributes(continueUser, model);
        return "user/addedbooks";
    }

    //просмотр истории обмена
    @RequestMapping(value = { "/user/exchange" }, method = RequestMethod.GET)
    public String userExchange(Model model) {
        Users continueUser = getCurrentLoggedUser();
        List<Histories> histories =  historiesService.findByUser(continueUser);
        model.addAttribute("userHistories", histories);

        Attributes.setAttributes(continueUser, model);
        return "user/exchange";
    }
}
