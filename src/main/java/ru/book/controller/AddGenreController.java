package ru.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.book.models.Genres;
import ru.book.service.GenresService;

@Controller
public class AddGenreController {
    @Autowired
    GenresService genresService;

    @GetMapping("/user/addnewgenre")
    public String userAddBookGet(Model model){
        Genres genre = new Genres();
        model.addAttribute("genre", genre);
        return "user/addnewgenre";
    }
    @PostMapping("/user/addnewgenre")
    public String userAddBookPost(Genres genre, Model model){
        genresService.save(genre);
        return "redirect:/user/addnewbook";
    }
}
