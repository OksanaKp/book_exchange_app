package ru.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.book.models.*;
import ru.book.service.*;

import java.util.List;

@Controller
public class catalogController {
    @Autowired
    BookService bookService;
    @Autowired
    UserService userService;
    @Autowired
    GenresService genresService;
    @Autowired
    PublisherService publisherService;
    @Autowired
    ActionService actionService;
    @Autowired
    HistoriesService historiesService;


    @GetMapping("/main/index")
    public String getIndex(Model model) {
        String mailLoggedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Users user = userService.findByEmail(mailLoggedUser);
        setAttributes(user, model);
        return "main/index";
    }

    @GetMapping("/main/catalog")
    public String getAllBooks(Model model) {
        String mailLoggedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Users user = userService.findByEmail(mailLoggedUser);
        Actions action1 = actionService.findById((long) 1);
        Actions action2 = actionService.findById((long) 3);
        List<Histories>  histories =  historiesService.findSharedBooks(action1, action2);
        model.addAttribute("booksHistList", histories);
        setAttributes(user, model);
        return "main/catalog";
    }


    @PostMapping("/main/catalog")
    public String getIndexFile(@RequestParam Long genref, @RequestParam Long publisherf, Model model) {
        String mailLoggedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Users user = userService.findByEmail(mailLoggedUser);
        Actions action1 = actionService.findById((long) 1);
        Actions action2 = actionService.findById((long) 3);
        List<Histories> histories;
        //поиск задан только по жанру
        if ((genref != null && genref != 0) && (publisherf==null || publisherf == 0)){
            Genres genre = genresService.getById(genref);//new Genres();
            histories =  historiesService.findSharedBooksByGenre(action1, action2, genre);
        }
        else
        //поиск задан только по издательству
        if ((genref == null || genref == 0) && (publisherf != null && publisherf != 0)) {
            Publishers publisher = publisherService.getById(publisherf);
            histories =  historiesService.findSharedBooksByPublishers(action1, action2, publisher);
        }
        //поиск задан и по жанру и по издательству
        else if ((genref != null && genref != 0) && (publisherf !=null && publisherf != 0)){
            Genres genre = genresService.getById(genref);
            Publishers publisher = publisherService.getById(publisherf);
            histories =  historiesService.findSharedBooksByGenreAndPublishers(action1, action2, genre, publisher);
        }
        else{
        //поиск не задан
            histories =  historiesService.findSharedBooks(action1, action2);
        }

        model.addAttribute("booksHistList", histories);

        setAttributes(user, model);
        return "main/catalog";
    }

    private void setAttributes(Users user, Model model){
        Attributes.setAttributes(user, model);
        List<Genres> genres = genresService.getAll();
        List<Publishers> publishers = publisherService.getAll();
        model.addAttribute("genresList", genres);
        model.addAttribute("publishersList", publishers);
    }
}
