package ru.book.controller;

import org.springframework.ui.Model;
import ru.book.models.Users;

public class Attributes {
    public static void setAttributes(Users user, Model model){
        if (user != null){
            model.addAttribute("logoutMessage", "logout");
            if (user.getRole().equals("admin")) {
                model.addAttribute("adminMessage", "Пользователи");
                model.addAttribute("adminMessageBooks", "Книги");
            }
            else {
                model.addAttribute("adminMessage", "");
                model.addAttribute("adminMessageBooks", "");
            }
        }
        else {
            model.addAttribute("logoutMessage", "");
        }
    }
}
