package ru.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.book.models.Books;
import ru.book.models.Genres;
import ru.book.models.Publishers;
import ru.book.models.Users;
import ru.book.service.BookService;
import ru.book.service.GenresService;
import ru.book.service.PublisherService;
import ru.book.service.UserService;

import java.util.List;

@Controller
@PreAuthorize("hasAuthority('admin')")
public class adminController {
    @Autowired
    BookService bookService;
    @Autowired
    UserService userService;
    @Autowired
    GenresService genresService;
    @Autowired
    PublisherService publisherService;


    // РАБОТА СО СПИСКОМ ПОЛЬЗОВАТЕЛЕЙ

    @GetMapping("/admin/users")
    public String getAllUsers(Model model) {
        String mailLoggedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Users user = userService.findByEmail(mailLoggedUser);
        List<Users> users = userService.getAll();
        model.addAttribute("usersList", users);
        model.addAttribute("logoutMessage", "logout");
        setAttributes(user, model);
        return "admin/users";
    }

    @PostMapping("/admin/users")
    public String getUsersByLastname(@RequestParam String lastName, Model model) {
        List<Users> users;
        if (lastName.isEmpty())
            users = userService.findAll();
        else
            users = userService.findByLastName(lastName);
        model.addAttribute("usersList", users);
        return "admin/users";
    }


    // РАБОТА С КАТАЛОГОМ

    @GetMapping("/admin/catalog")
    public String getAllBooks(Model model) {
        String mailLoggedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Users user = userService.findByEmail(mailLoggedUser);
        List<Books> books = bookService.getAll();
        model.addAttribute("booksList", books);
        model.addAttribute("logoutMessage", "logout");
        setAttributes(user, model);
        return "admin/catalog";
    }


    @PostMapping("/admin/catalog")
    public String getIndexFile(@RequestParam Long genref, @RequestParam Long publisherf, Model model) {
        String mailLoggedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Users user = userService.findByEmail(mailLoggedUser);
        Iterable<Books> books;
       //поиск задан только по жанру
        if ((genref != null && genref != 0) && (publisherf==null || publisherf == 0)){
            Genres genre = genresService.getById(genref);//new Genres();
            books = bookService.findByGenre(genre);
        }
        else
            //поиск задан только по издательству
            if ((genref == null || genref == 0) && (publisherf != null && publisherf != 0)) {
                Publishers publisher = publisherService.getById(publisherf);
                books = bookService.findByPublisher(publisher);
            }
            //поиск задан и по жанру и по издательству
            else if ((genref != null && genref != 0) && (publisherf !=null && publisherf != 0)){
                Genres genre = genresService.getById(genref);
                Publishers publisher = publisherService.getById(publisherf);
                books = bookService.findByGenreAndPublisher(genre, publisher);
            }
            else{
                //поиск не задан
                books = bookService.getAll();
            }

        model.addAttribute("booksList", books);

        setAttributes(user, model);
        return "admin/catalog";
    }

    @GetMapping("/admin/catalog/delete/{id}")
    public String deleteToy(@PathVariable("id") Long id){
        bookService.deleteById(id);
        return "redirect:/admin/catalog";
    }


    private void setAttributes(Users user, Model model) {
        Attributes.setAttributes(user, model);
        List<Genres> genres = genresService.getAll();
        List<Publishers> publishers = publisherService.getAll();
        model.addAttribute("genresList", genres);
        model.addAttribute("publishersList", publishers);
    }
}