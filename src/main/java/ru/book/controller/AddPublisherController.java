package ru.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.book.models.Publishers;
import ru.book.service.PublisherService;

@Controller
public class AddPublisherController {
    @Autowired
    PublisherService publisherService;

    @GetMapping("/user/addnewpublisher")
    public String userAddBookGet(Model model){
        Publishers publisher = new Publishers();
        model.addAttribute("publisher", publisher);
        return "user/addnewpublisher";
    }
    @PostMapping("/user/addnewpublisher")
    public String userAddBookPost(Publishers publisher, Model model){
        publisherService.save(publisher);
        return "redirect:/user/addnewbook";
    }
}
