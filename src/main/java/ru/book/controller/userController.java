package ru.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.book.models.Users;
import ru.book.service.UserService;

@Controller
public class userController {

    @Autowired
    UserService userService;

    @RequestMapping(value = { "/user/account" }, method = RequestMethod.GET)
    public String userAccount(Model model) {
        Authentication authenticationUser = SecurityContextHolder.getContext().getAuthentication();//.getPrincipal()
        String mailLoggedUser = authenticationUser.getName();
        Users user = userService.findByEmail(mailLoggedUser);
        model.addAttribute("loginnedUser", user);
        Attributes.setAttributes(user, model);
        return "user/myaccount";
    }

    @GetMapping("/user/edit")
    public String updateUserForm(Model model){
        Authentication authenticationUser = SecurityContextHolder.getContext().getAuthentication();//.getPrincipal()
        String mailLoggedUser = authenticationUser.getName();
        Users user = userService.findByEmail(mailLoggedUser);
        model.addAttribute("continueUser", user);
        Attributes.setAttributes(user, model);
        return "user/edit";
    }

    @PostMapping("/user/edit")
    public String updateUser(Users user){
        userService.save(user);
        return "redirect:/user/account";
    }
}
