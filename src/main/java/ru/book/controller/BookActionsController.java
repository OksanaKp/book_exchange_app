package ru.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.book.models.*;
import ru.book.service.*;

import java.util.Date;

@Controller
public class BookActionsController {
    @Autowired
    UserService userService;

    @Autowired
    HistoriesService historiesService;

    @Autowired
    BookService bookService;

    @Autowired
    ActionService actionService;

    @Autowired
    GenresService genresService;

    @Autowired
    PublisherService publisherService;

    //определить текущего авторизованного пользователя
    private Users getCurrentLoggedUser() {
        Authentication authenticationUser = SecurityContextHolder.getContext().getAuthentication();
        String mailLoggedUser = authenticationUser.getName();
        Users continueUser = userService.findByEmail(mailLoggedUser);
        return continueUser;
    }

    //отправка формы для добавления новой книги
    @GetMapping("/user/addnewbook")
    public String userAddBookGet(Model model){
        Books book = new Books();
        Iterable<Genres> genres = genresService.getAll();
        Iterable<Publishers> publishers = publisherService.getAll();
        model.addAttribute("book", book);
        model.addAttribute("genresList", genres);
        model.addAttribute("publishersList", publishers);
        return "user/addnewbook";
    }

    //получение данных для добавления новой книги
    @PostMapping("/user/addnewbook")
    public String userAddBookPost(Books book, Model model){
        Users continueUser = getCurrentLoggedUser();
        bookService.save(book);
        Date date = new Date();
        Actions action = actionService.findById((long) 1);
        Histories history = new Histories(continueUser, book, action, date);
        historiesService.save(history);
        return "redirect:/user/added";
    }

    //выдача книги пользователю
    @PostMapping(value = { "/user/addgetaction" })
    public String userGetBook(@RequestParam Long bookId, Model model) {
        Users continueUser = getCurrentLoggedUser();
        Books book = bookService.findById(bookId);
        Date date = new Date();
        Actions action = actionService.findById((long) 2);
        Histories history = new Histories(continueUser, book, action, date);
        historiesService.save(history);
        return "redirect:/user/exchange";
    }

    //возврат книги на раздачу
    @PostMapping(value = { "/user/returnaction" })
    public String userReturnBook(@RequestParam Long bookId, Model model) {
        Users continueUser = getCurrentLoggedUser();
        Books book = bookService.findById(bookId);
        Date date = new Date();
        Actions action = actionService.findById((long) 3);
        Histories history = new Histories(continueUser, book, action, date);
        historiesService.save(history);
        return "redirect:/user/exchange";
    }
}
