package ru.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.book.models.Users;
import ru.book.service.UserService;

@Controller
public class registrationController {
    @Autowired
    private UserService userService;
    @GetMapping("/registration")
    public String reg(Model model) {
        Users user = new Users();
        model.addAttribute("newUser", user);
        return "registration";
    }

    @PostMapping("/registration")
    public String regAddNewUser(Users user, Model model) {
        Users existsUser = userService.findByEmail(user.getEmail());
        if (existsUser != null){
            user = new Users();
            model.addAttribute("newUser", user);
            model.addAttribute("errMessage","Логин занят!");
            return "registration";
        }
        user.setRole("user");
        userService.save(user);
        return "redirect:/login";
    }
}
